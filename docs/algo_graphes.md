# Algorithmes sur les graphes

Ainsi que pour les arbres, les premiers algorithmes importants sont les algorithmes de parcours.

## Rappel algorithme de parcours d'arbre binaire

Voici l'algorithme de **parcours en profondeur** d'un arbre binaire.

``` linenums="1"
Procédure explorer
Explore les noeuds de l'arbre selon un parcours en profondeur
Arguments 1 : arbre binaire A
Pas de valeur de retour

placer A dans une pile
tant que la pile n'est pas vide
    dépiler N
    traiter N (afficher)
    empiler si ils existent les sous-arbres gauche et droite
```

??? example "Dans quel ordre sont parcourus les noeuds de l'arbre ci-dessous en utilisant l'algorithme de parcours ?"
    A C G F B E D

    Voici le tableau des états que l'on peut construire si on fait tourner à la main l'algorithme. E/S indique l'entrée-sortie de la pile.

    |   pile  |  N  |pile vide ?|
    |:--------|:---:|:---------:|
    |E/S A    |  ?  |    Non    |
    |E/S      |  A  |           |
    |E/S C B  |     |    Non    |
    |E/S B    |  C  |           |
    |E/S G F B|     |    Non    |
    |E/S F B  |  G  |    Non    |
    |E/S B    |  F  |    Non    |
    |E/S      |  B  |           |
    |E/S E D  |     |    Non    |
    |E/S D    |  E  |    Non    |
    |E/S      |  D  |    Oui    |

``` mermaid
graph TD
  A((A)) --- B((B))
  A((A)) --- C((C))
  B((B)) --- D((D))
  B((B)) --- E((E))
  C((C)) --- F((F))
  C((C)) --- G((G))
```

Maintenant modifions et appliquons cet algorithme au graphe suivant.

``` mermaid
graph TD
  A((A)) --- B((B))
  A((A)) --- C((C))
  B((B)) --- D((D))
  B((B)) --- E((E))
  C((C)) --- F((F))
  C((C)) --- G((G))
  E((E)) --> A((A))
```
??? example "Ecrire l'algorithme de parcours de l'arbre en l'adaptant à un graphe : il n'y a plus de notion de sous arbre; il faut choisir un sommet de départ."
    ``` linenums="1"
    Procédure explorer
    Explore les sommets du graphe selon un parcours en profondeur
    Arguments 2 : graphe G, sommet de départ A
    Pas de valeur de retour

    placer A dans une pile
    tant que la pile n'est pas vide
        dépiler N
        traiter N (afficher)
        empiler les voisins de A dans le graphe G
    ```

??? warning "Quel est le problème posé par cette situation et cet algorithme ?"
    
    L'algorithme ne va jamais terminer.

    L'ordre de traiment des sommets est **A C G F B E** A C G F B E A C ...  
    On observe une période dans l'affichage A C G F B E; le sommet D n'est jamais parcouru.

    La solution sera de **marquer** les sommets déjà traités pour ne pas à avoir à les traiter par la suite.

## Algorithme de parcours dans les graphes - le marquage

### Parcours en profondeur

Un **parcours en profondeur** suit un chemin; puis si le chemin ne peut se poursuivre, remonte à la première bifurcation possible.

??? example "Modifier l'algorithme de parcours précédent pour qu'il se termine en prenant en compte la notion de sommet marqué ou pas ?"  

    ``` linenums="1"
    Procédure explorer
    Explore les sommets du graphe selon un parcours en profondeur
    Arguments 2 : graphe G, sommet de départ A
    Pas de valeur de retour

    placer A dans une pile
    tant que la pile n'est pas vide
        dépiler N
        marquer N
        traiter N (afficher)
        empiler les voisins de A non marqués dans le graphe G
    ```

Faisons tourner l'algorithme à la main. La couleur rouge indique que le sommet est marqué.

=== "1"

    La pile contient A.  
    E/S A  
    On dépile A.  
    E/S Vide

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    ```
    On empile les voisins non marqués de A.  
    E/S C B

=== "2"

    Etat de la pile :  
    E/S C B  
    On dépile C.  
    E/S B

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    style C fill:#BB0000
    ```
    On empile les voisins non marqués de C.  
    E/S G F B

=== "3"

    Etat de la pile :  
    E/S G F B  
    On dépile G.  
    E/S F B

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    style C fill:#BB0000
    style G fill:#BB0000
    ```
    On empile les voisins non marqués de G. Il n'y en a pas.  
    E/S F B


=== "4"

    Etat de la pile :  
    E/S F B  
    On dépile F.  
    E/S B

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    style C fill:#BB0000
    style F fill:#BB0000
    style G fill:#BB0000
    ```
    On empile les voisins non marqués de F. Il n'y en a pas.  
    E/S B

=== "5"

    Etat de la pile :  
    E/S B  
    On dépile B.  
    E/S Vide

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    style B fill:#BB0000
    style C fill:#BB0000
    style F fill:#BB0000
    style G fill:#BB0000
    ```
    On empile les voisins non marqués de B.  
    E/S E D

=== "6"

    Etat de la pile :  
    E/S E D  
    On dépile E.  
    E/S D

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    style B fill:#BB0000
    style C fill:#BB0000
    style E fill:#BB0000
    style F fill:#BB0000
    style G fill:#BB0000
    ```
    On empile les voisins non marqués de E. Il n'y en a pas.   
    E/S D

=== "7"

    Etat de la pile :  
    E/S D  
    On dépile D.  
    E/S Vide

    ``` mermaid
    graph TD
    A((A)) --- B((B))
    A((A)) --- C((C))
    B((B)) --- D((D))
    B((B)) --- E((E))
    C((C)) --- F((F))
    C((C)) --- G((G))
    E((E)) --> A((A))
    style A fill:#BB0000
    style B fill:#BB0000
    style C fill:#BB0000
    style D fill:#BB0000
    style E fill:#BB0000
    style F fill:#BB0000
    style G fill:#BB0000
    ```
    On empile les voisins non marqués de D. Il n'y en a pas.   
    E/S Vide. L'algorithme se termine.

??? example "Pourquoi doit on marquer les sommets dans un parcours de graphe ?"
    
    Les algorithmes de parcours ne doivent pas suivre des cycles. Les sommmets sont marqués pour ne plus avoir à être explorés.

### Parcours en largeur

Dans un **parcours en largeur**, les sommets sont explorés selon leur distance au noeud de départ. On explore couche par couche.

??? example "Que faut-il Modifier dans l'algorithme de parcours en profondeur pour en faire un algorithme de parcours en largeur ? Faire tourner l'algorithme sur le graphe précédent."  

    ``` linenums="1"
    Procédure explorer
    Explore les sommets du graphe selon un parcours en largeur
    Arguments 2 : graphe G, sommet de départ A
    Pas de valeur de retour

    placer A dans une file
    tant que la file n'est pas vide
        défiler N
        marquer N
        traiter N (afficher)
        enfiler les voisins de A non marqués dans le graphe G
    ```

    Faisons tourner l'algorithme à la main. La couleur rouge indique que le sommet est marqué. E désigne l'entrée de la file et S sa sortie.

    === "1"

        La file contient A.  
        E A S  
        On défile A.  
        E Vide S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        ```
        On enfile les voisins non marqués de A.  
        E B C S

    === "2"

        Etat de la file :  
        E C B S  
        On défile B.  
        E C S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        style B fill:#BB0000
        ```
        On enfile les voisins non marqués de B.  
        E E D C S

    === "3"

        Etat de la file :  
        E E D C S  
        On défile C.  
        E E D S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        style B fill:#BB0000
        style C fill:#BB0000
        ```
        On enfile les voisins non marqués de C.  
        E G F E D S


    === "4"

        Etat de la file :  
        E G F E D S  
        On défile D.  
        E G F E S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        style B fill:#BB0000
        style C fill:#BB0000
        style D fill:#BB0000
        ```
        On enfile les voisins non marqués de D. Il n'y en a pas.  
        E G F E S

    === "5"

        Etat de la file :  
        E G F E S  
        On défile E.  
        E G F S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        style B fill:#BB0000
        style C fill:#BB0000
        style D fill:#BB0000
        style E fill:#BB0000
        ```
        On enfile les voisins non marqués de E. Il n'y en a pas.  
        E G F S

    === "6"

        Etat de la file :  
        E G F S  
        On défile F.  
        E G S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        style B fill:#BB0000
        style C fill:#BB0000
        style D fill:#BB0000
        style E fill:#BB0000
        style F fill:#BB0000
        ```
        On enfile les voisins non marqués de F. Il n'y en a pas.   
        E G S

    === "7"

        Etat de la file :  
        E G S  
        On défile G.  
        E Vide S

        ``` mermaid
        graph TD
        A((A)) --- B((B))
        A((A)) --- C((C))
        B((B)) --- D((D))
        B((B)) --- E((E))
        C((C)) --- F((F))
        C((C)) --- G((G))
        E((E)) --> A((A))
        style A fill:#BB0000
        style B fill:#BB0000
        style C fill:#BB0000
        style D fill:#BB0000
        style E fill:#BB0000
        style F fill:#BB0000
        style G fill:#BB0000
        ```
        On enfile les voisins non marqués de G. Il n'y en a pas.   
        E Vide S. L'algorithme se termine.

    ??? example "Pourquoi doit on marquer les sommets dans un parcours de graphe ?"
        
        Les algorithmes de parcours ne doivent pas suivre des cycles. Les sommmets sont marqués pour ne plus avoir à être explorés.

## Algorithme de recherche du chemin le plus court

Le graphe suivant est un graphe pondéré. Une distance est associée à chaque arête.

``` mermaid
graph TD
    A(("A(0)")) ---|85| B(("B(inf)"))
    A ---|217| C(("C(inf)"))
    A ---|173| E(("E(inf)"))
    B ---|80| F(("F(inf)"))
    C ---|217| G(("G(inf)"))
    C ---|103| H(("H(inf)"))
    D(("D(inf)")) ---|183| H
    E ---|502| J(("J(inf)"))
    F ---|250| I(("I(inf)"))
    J ---|167| H
    I ---|84| J
```

### Algorithme de Dijkstra des graphes pondérés

!!! info "Edsger Dijstra"

    Cet algorithme fut inventé et publié par l'informaticien néerlandais Edsger Dijkstra en 1959. Cet algorithme est utilisé dans les protocoles de routage tels que Open Shortest Path First OSPF.  
    E.Dijkstra est également connu pour avoir combattu l'utilisation de l'instruction GOTO (Quand un code est peu clair par l'usage excessif de sauts inconditionnels, on parle de code spaghetti.) au profit de la programmation structurée. Il reçoit en 1972 le prix Turing.

L'algorithme de dijkstra est un parcours en largeur. Néanmoins, on n'utilise pas de file mais un tableau.

Le sommet sorti du tableau ( et marqué) est celui dont la distance au sommet de départ est la plus courte.

Quand un sommet doit être ajouté au tableau, sa distance au sommet de départ est mise à jour.  En effet, plusieurs chemins peuvent exister pour le rejoindre à partir du sommet de départ. Pendant l'exécution du parcours en largeur, un parcours plus rapide peut être découvert.

Voici l'algorithme de Dijstra.

``` linenums="1"
    Procédure plus-court_chemin_dijikstra
    Détermine le plus court chemin entre deux sommets dans un graphe pondéré.
    Arguments 3 : graphe pondéré G, sommet de départ A, sommet d'arrivée B  
    La liste de sommets

    Attribuer une distance d infinie à tous les sommets du graphes
    Les sommets n'ont pas de précécesseur
    dA <- 0
    placer A dans un tableau
    tant que le tableau n'est pas vide
        Sortir N du tableau, N étant le sommet ayant la plus petite distance à A
        marquer N
        Pour chaque sommet S voisin de N et non marqué
            Si dS > dN + poids arête NS alors
                dS <- dA + poids arête NS
                le précécesseur de S est N
        enfiler les voisins de A non marqués dans le graphe G
    ```

    