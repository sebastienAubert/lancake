# Les Graphes - Term NSI

## Qu'est ce qu'un graphe ?

Un **graphe** est un ensemble de **sommets** reliés par des **arêtes**.

``` mermaid
graph LR
  1((1)) --- 2((2));
  1((1)) --- 5((5));
  2((2)) --- 5((5));
  2((2)) --- 3((3));
  5((5)) --- 4((4));
  3((3)) --- 4((4));
  4((4)) --- 6((6));
```

??? example "Combien de sommets et d'arêtes possède ce graphe ?"
    Ce graphe a 6 sommets reliés par 7 arêtes.

Cela peut servir à modéliser des réseaux informatiques, routiers ...

Il est alors utile d'attribuer des valeurs aux arêtes et aux somments (on dit qu'ils sont **pondérés**).

## Modélisation d'un graphe par une matrice d'adjacence ou des listes d'adjacence

Adjacence signifie que cette matrice ou ces listes indiqueront les sommets qui sont liés.

### **Listes d'adjacence**

| Sommet de départ | Sommets d'arrivée|
|:----------------:|:-----------------|
|1|5,2|
|2|1,5,3|
|3|2,4|
|4| ... |
|5|1,2,4|
|6|4|

Pour chaque sommet, il est indiqué la liste des noeuds adjacents (liés).

??? example "Quelle est la liste pour le sommet de départ 4 ?"
    La liste est 3,5,6.

### **Matrice d'adjacence**

|     |1    |2    |3    |4    |5    |6|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|1|0|1|0|0|1| ... |
|2|1|0|1|0|1| ... |
|3|0|1|0|1|0| ... |
|4|0|0|1|0|1| ... |
|5|1|1|0|1|0| ... |
|6|0|0|0|1|0| ... |

La première ligne indique les sommets d'arrivée, la première colonne les sommets de départ.

1 est placé dans la matrice (tableau) quand une arête existe sinon 0.

On remarque que la matrice est symétrique par rapport à la diagonale.  
C'est tout à fait normal car si 1 est lié à 5 alors 5 est lié à 1.

??? example "Quel est la dernière colonne de la matrice d'ajacence ?"
    |6|
    |:---:|
    |0|
    |0|
    |0|
    |1|
    |0|
    |0|

## Les graphes orientés

Les arêtes peuvent être orientées; on parle alors d'arcs. Le graphe est alors dit orienté.

``` mermaid
graph LR
  A((A)) --> B((B));
  B((B)) --> C((C));
  C((C)) --> E((E));
  E((E)) --> F((F));
  E((E)) --> D((D));
  D((D)) --> B((B));
```
??? example "Ecire les listes d'adjacence et la matrice d'adjacence de ce graphe orienté."
    
    Liste d'adjacence

    Pour chaque sommet, il est indiqué la liste des noeuds adjacents (liés par l'arc). Une arête d'un graphe non orienté peut être considérée comme deux arcs.

    | Sommet de départ | Sommets d'arrivée|
    |:----------------:|:-----------------|
    |A|B|
    |B|C|
    |C|E|
    |D|B|
    |E|F,D|
    |F||

    |     |A    |B    |C    |D    |E    |F|
    |:---:|:---:|:---:|:---:|:---:|:---:|:---:|
    |A|0|1|0|0|0|0|
    |B|0|0|1|0|1|0|
    |C|0|0|0|0|1|0|
    |D|0|1|0|0|0|0|
    |E|0|0|0|1|0|1|
    |F|0|0|0|0|0|0|

    La matrice n'est plus symétrique.

## Méthodes usuelles du type abstrait

Le type abstrait graphe orienté doit possèder les méthodes suivantes:

- Créer un graphe
- Deux sommets sont-ils voisins ?
- Liste des voisins d'un sommet
- Ajouter un sommet
- Supprimer un sommet
- Ajouter une arête
- Supprimer une arête

## Implémentation en Python

Dans cette introduction aux graphes, il ne sera proposé qu'une implémentation simple sans POO. Une connaissance correcte des dictionnaires et les listes est nécessaire.

Nous souhaitons implémenter le graphe G suivant.

``` mermaid
graph LR
  1((1)) --- 2((2));
  1((1)) --- 5((5));
  2((2)) --- 5((5));
  2((2)) --- 3((3));
  5((5)) --- 4((4));
  3((3)) --- 4((4));
  4((4)) --- 6((6));
```

### Création à partir d'un dictionnaire

Les listes d'adjacences sont aisément implémentés via un dictionnaire dont les clés sont les sommets et les valeurs, la liste des sommets liés.

```python
G={ 1 :[2,5],
    2 :[1,3,5],
    3 :[2,4],
    ... }
```

??? example "Ecrire complétement le dictionnaire G implémentant le graphe."
    ```python
    G = {1:[2,5],
         2:[1,3,5],
         3:[2,4],
         4:[3,5,6],
         5:[1,2,4],
         6:[4]}
    ```

### Liste des voisins d'un sommet

La liste est la valeur dont la clé est le sommet.

??? example "Quel script ou fonction peut-on écrire pour optenir la liste des voisins du sommet 5 du graphe G ?"
    
    - script
    
    ```python
    liste=list(G[7]) # pour en faire une copie
    ```

    - fonction
    
    ```python
    def voisins(G,A):
      return list(G[A]) # pour en faire une copie

    # Appel
    liste=voisins(G,7)
    ```

### Deux sommets sont-ils voisins ?

Il faut vérifier une liste si le graphe est non orienté et deux listes si le graphe est orienté.

??? example "Quel script ou fonction peut-on écrire pour vérifier la proximité des sommets 6 et 3 du graphe G ?"
    
    - script
    
    ```python
    7 in G[6] or 6 in G[7]
    ```

    - fonction

    ```python
    def sontVoisins(G,A,B):
      return A in G[B] or B in G[A]

    # Appel
    reponse=sontVoisins(G,6,7)
    ```

### Ajout d'un sommet

On ajoute comme clé le sommet au dictionnaire avec pour valeur une liste vide.

??? example "Quel script ou procédure peut-on écrire pour ajouter le sommet 7 au graphe G ?"
    
    - script
    
    ```python
    G[7]=[]
    ```

    - procédure

    ```python
    def ajout(G,A):
      # il faudrait vérifier qu'il n'existe pas
      G[A]=[]

    # Appel
    ajout(G,7)
    ```

### Ajout d'une arête

On ajoute à la liste d'un sommet un autre sommet. Ne pas oublier de le faire deux fois, la deuxième avec les sommets inversés.

??? example "Quel script ou procédure peut-on écrire pour ajouter une arête entre 1 et 6 sur le graphe G ?"
    
    - script
    
    ```python
    G[1].append(6)
    G[6].append(1)
    ```

    - procédure

    ```python
    def ajoutArete(G,A,B):
      # il faudrait vérifier qu'elles ne sont pas présentes
      G[A].append(B)
      G[B].append(A)

    # Appel
    ajoutArete(G,1,6)
    ```


### Suppression d'une arête

Quand on supprime une arête, il faut retirer un sommet de la liste d'un autre et réciproquement.

??? example "Quel script ou procédure peut-on écrire pour supprimer l'arête entre les sommets 2 et 5 du graphe G ?"
    
    - script
    
    ```python
    G[2].remove(5)
    G[5].remove(2)
    ```

    - procédure

    ```python
    def supprimeArete(G,A,B):
      # il faut vérifier qu'elles sont bien là
      G[A].remove(B)
      G[B].remove(A)

    # Appel
    supprimeArete(G,2,5)
    ```

### Suppression d'un sommet

Il ne faut pas oublier de supprimer toutes les arêtes.

??? example "Quel script ou procédure peut-on écrire pour supprimer le sommet 2 du graphe G ?"
    
    - script
    
    ```python
    liste=voisins(G,2)
    for e in liste:
        G[e].remove(2)
        G[2].remove(e)
    del G[2]
    ```

    - procédure

    ```python
    def supprimeSommet(G,A):
      # il faut vérifier que le sommet est bien présent
      liste=voisins(G,A)
      for e in liste:
          # il faudrait supprimer l'arête correspondante
          G[e].remove(A)
          G[A].remove(e)
      del G[A]
    ```

    
