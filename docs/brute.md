# Les algorithmes force brute

## Qu'est ce qu'un problème d'optimisation ?

Exemple du problème du sac à dos:

Un sac à dos de randonnée peut contenir un certain nombre d'objets. Chaque objet présente une utilité pour la randonnée.
La modélisation du problème consiste à modéliser la taille du sac à dos, la place et l'utilité de chaque objet via des valeurs numériques.
Il y a des contraintes; la place prise par les objets ne peut être suppérieure à la taille du sac.
Il y a une grandeur à optimiser; la somme des utilités doit être maximale.

Un **problème d'optimisation** consiste à minimiser ou maximiser une fonctions sur un ensemble.

Dans le cas du sac à dos, l'ensemble est celui des objets et la fonction à maximiser est l'utilité totale.

## Qu'est ce que la force brute ?

Utiliser la puissance de calcul d'un ordinateur pour calculer la fonction pour TOUS LES CAS. Puis de chercher le cas pour lequel la fonction est minimale ou maximale est appelé **algorithme de force brute**.

La méthode est simple

1. Etablir l'ensemble de tous les cas
2. Calculer la fonction pour tous les cas
3. Chercher les cas pour lequel la fonction est minimale ou maximale

On peut écrire l'algorithme suivant

``` linenums="1"
maximum <- 0
solution <- Aucune
Pour tous les cas e à traiter faire
    Si e respecte les contraintes et si e est une meilleure solution alors
        maximum <- fonction(e)
        solution <- e
Afficher e
```

## Quels outils pour générer tous les cas ?

On peut créer un algorithme spécifique néamoins la bibliothèque itertools fournit des fonctions utiles.
On considère les choix possibles A, B, C et D.

### Produit cartésien

Les éléménts de l'ensemble peuvent être choisit N fois et l'ordre de ces choix compte.
On utilise l'itérateur **product** de itertools.

```python
import itertools as it

for e in it.product('ABCD',repeat=2):
    print(e)
```
??? example "Affichage en console"
    ('A', 'A')  
    ('A', 'B')  
    ('A', 'C')  
    ('A', 'D')  
    ('B', 'A')  
    ('B', 'B')  
    ('B', 'C')  
    ('B', 'D')  
    ('C', 'A')  
    ('C', 'B')  
    ('C', 'C')  
    ('C', 'D')  
    ('D', 'A')  
    ('D', 'B')  
    ('D', 'C')  
    ('D', 'D')  

### Permutations

Les éléments peuvent être choisit sans répétitions et l'ordre de ces choix compte.
On utilise l'itérateur **permutation** de itertools.

```python
import itertools as it

for e in it.permutations('ABCD',2):
    print(e)
```

??? example "Affichage en console"
    ('A', 'B')  
    ('A', 'C')  
    ('A', 'D')  
    ('B', 'A')  
    ('B', 'C')  
    ('B', 'D')  
    ('C', 'A')  
    ('C', 'B')  
    ('C', 'D')  
    ('D', 'A')  
    ('D', 'B')  
    ('D', 'C')  

### Combinaisons

Les éléments peuvent être choisit sans répétition et l'ordre de ces choix ne compte pas.
On utilise l'itérateur **combinations** de itertools.

```python
import itertools as it

for e in it.combinations('ABCD',2):
    print(e)
```

??? example "Affichage en console"
    ('A', 'B')  
    ('A', 'C')  
    ('A', 'D')  
    ('B', 'C')  
    ('B', 'D')  
    ('C', 'D')  

### Combinaisons avec remise

Les éléments peuvent être choisit avec répétition et l'odre de ces choix ne compte pas.
On utilise l'itérateur **combinations_with_replacement** de itertools.

```python
import itertools as it

for e in it.combinations_with_replacement('ABCD',2):
    print(e)
```

??? example "Affichage en console"
    ('A', 'A')  
    ('A', 'B')  
    ('A', 'C')  
    ('A', 'D')  
    ('B', 'B')  
    ('B', 'C')  
    ('B', 'D')  
    ('C', 'C')  
    ('C', 'D')  
    ('D', 'D')  


## Exemple du problème du sac à dos

Voici l'ensemble des objets que nous pouvons choisir pour un sac à dos :

| Objet |   A   |   B   |   C   |   D   |   E   |   F   |   G   |   H   |   I   |   J   |   K   |   L   |   M   |   N   |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|Valeur |   4   |   3   |   8   |   5   |  10   |   7   |   1   |   7   |   3   |   3   |   6   |  12   |   2   |   4   |
|Poids  |   2   |   2   |   5   |   2   |   7   |   4   |   1   |   4   |   2   |   1   |   4   |  10   |   2   |   1   |

Le sac à dos a une capacité de 26.
Il y a 14 objets.

??? example "Autre exemple (A traiter en exercice)"
    | Objet |   A   |   B   |   C   |   D   |   E   |   F   |
    |:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
    |Valeur |  30   |  12   |  12   |  12   |  12   |   4   |
    | Poids |  39   |  10   |  10   |  10   |  10   |   1   |
    
    ```python
    objets={"A":[30,39],"B":[12,10],"C":[12,10],"D":[12,10],"E":[12,10],"F":[4,1]}
    ```
    On prendra une contenance de 40 pour le sac à dos

On peut prendre entre 0 et 14 objets. On ne peut pas prendre plusieurs fois le même objet. Nous allons utiliser l'itérateur combinations.

On utilise un dictionnaire pour les objets dont les clés sont les lettres.

Pour généner les choix possibles.

```python
import itertools as it

objets={"A":[4,2],"B":[3,2],"C":[8,5],"D":[5,2],"E":[10,7],"F":[7,4],"G":[1,1],"H":[7,4]
       ,"I":[3,2],"J":[3,1],"K":[6,4],"L":[12,10],"M":[2,2],"N":[4,1]}

for i in range(0,27):
    for e in it.combinations('ABCDEFGHIJKLMN',i):
        ...
```

Fonction dont la valeur doit être maximale

```python
def fonction(liste,objets):
    somme=0
    for e in liste:
        somme=somme+objets[e][0]
    return somme
```

Contrainte la somme des poids doit être inférieure à la taille du sac

```python
def contrainte(liste,objets,taille):
    somme=0
    for e in liste:
        somme=somme+objets[e][1]
    if somme>taille:
        return False
    else:
        return True
```

Le script final doit chercher une solution optimale.

```python
import itertools as it

def fonction(liste,objets):
    somme=0
    for e in liste:
        somme=somme+objets[e][0]
    return somme

def contrainte(liste,objets,taille):
    somme=0
    for e in liste:
        somme=somme+objets[e][1]
    if somme>taille:
        return False
    else:
        return True

objets={"A":[4,2],"B":[3,2],"C":[8,5],"D":[5,2],"E":[10,7],"F":[7,4],"G":[1,1]
       ,"H":[7,4],"I":[3,2],"J":[3,1],"K":[6,4],"L":[12,10],"M":[2,2],"N":[4,1]}

maxi=0
solution=None
for i in range(0,27):
    print(i)
    for e in it.combinations('ABCDEFGHIJKLMN',i):
        if contrainte(e,objets,26) and fonction(e,objets)>=maxi:
            maxi=fonction(e,objets)
            solution=e

print(solution)
```

{{IDE()}}

## Quelques remarques sur la complexité de l'algorithme

Le nombre de cas à traiter dans le cas du sac à dos est $\sum\limits_{k=0}^{n} C_{n}^{k}=2^{n}$.  
Les algorithmes de calculs sont proportionnels à k; donc le temps de calcul est $\sum\limits_{k=0}^{n} k.C_{n}^{k}=n.2^{n-1}$.  
On est donc en $O(2^{n})$ ce qui croit beaucoup plus vite en temps de calcul que n'importe quel algorithme en $O(n^{a})$