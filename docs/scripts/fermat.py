"""
# Exercice :

1. Dans la définition de F(n),
    - remplacer ... par 2**( ... ) + 1,
    - et compléter.
2. Exécuter le script.
3. Dans la console, vérifier que :
    a. F(4) est égal à 65537.
    b. F(5) est divisible par 641.
    c. F(6) est divisible par 274177.
4. En déduire des facteurs de F(5) et F(6).
5. Afficher les 9 derniers chiffres de F(12).

"""

def F(n):
    """
    Renvoie le nombre de Fermat F_n :
    2 à la puissance (2 puissance n), plus un
    """
    return ...
