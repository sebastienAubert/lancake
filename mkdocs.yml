site_name: NSI Mermoz Mkdocs
site_url: https://sebastienaubert.gitlab.io/lancake/
use_directory_urls: false

nav: 
  - Introduction aux graphes : index.md
  - Algorithme sur les graphes : algo_graphes.md
  - SNT :
    - Introduction : snt.md
  - Programmation dynamique : brute.md


# Configuration du thème https://squidfunk.github.io/mkdocs-material/creating-your-site/#configuration
theme:
    name: material # autres thèmes : https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes
    custom_dir: docs/my_theme_customizations/
    font: false  # RGPD ; pas de fonte Google
    language: fr # français
    icon:
      logo: fontawesome/solid/code # image qui apparait dans l'en tête à gauche du titre de la page
      # et qui est support du lien qui renvoie à la racine du site depuis toutes les pages
      repo: fontawesome/brands/github-alt # modifie l'icône du lien vers le dépot du projet de l'en tête
    
    # Palettes de couleurs jour/nuit, cf : https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/
    palette: 
      - scheme: default # nom du thème clair
        primary: light-blue # couleur primaire des titres, des liens, ..., à prendre dans la liste
        accent: amber # couleur d'accentuation au survol des boutons, desliens, ..., à prendre dans la liste
        toggle: # définition du bouton pour switcher de palette
          icon: material/weather-sunny # apparence
          name: Basculer en mode sombre # message
      - scheme: slate # nom du thème sombre
        primary: blue
        accent: lime
        toggle:
          icon: material/weather-night
          name: Basculer en mode clair
    # Configuration du comportement de la navigation, cf : https://squidfunk.github.io/mkdocs-material/setup/setting-up-navigation/
    features: 
        - navigation.instant # active XHR https://fr.wikipedia.org/wiki/XMLHttpRequest
        - navigation.tabs # Menu de navigation horizontal sous l'en-tête sauf sur appareil mobile
        # - navigation.tabs.sticky # Menu de navigation horizontal collant
        - navigation.top # bouton pour remonter tout en haut de la page
        - toc.integrate # Table Of Content, table des matières intégrée dans la barre verticale de navigation
        - header.autohide # masquage automatique de l'en tête du site lorsque l'on descend dans la page


### Configuration des extensions MarkDown :        
markdown_extensions: # https://squidfunk.github.io/mkdocs-material/reference/abbreviations/
    # extensions de la bibliothèque markdown standard
    - abbr                          # Infobulles sur abbréviations 
    - meta                          # Métadonnées
    - def_list                      # Listes de définition.
    - attr_list                     # Sélecteurs CSS et attributs HTML personnalisés
    - md_in_html                    # Pour écrire en MarkDown dans des balises HTML https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/#markdown-in-html
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Boite d'avertissements  !!! info "ma remarque"
    - toc:                          # Configuration de la table des matières générée automatiquement à partir des titres du niveau 2
        permalink: "&num;"          # Ajoute un symbole lien hypertexte vers l'ancre du titre #le-titre 
        toc_depth: 4                # Limite de la profondeur d'inclusion des titres dans la table des matières
    # extensions de python-markdown https://facelessuser.github.io/pymdown-extensions/
    - pymdownx.details              # plier/déplier les avertissements.
    - pymdownx.caret                # texte ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # texte ==surligné==.
    - pymdownx.tilde                # texte ~~barré~~ ou en ~indice~.
    - pymdownx.critic               # Pour du marquage et commentaires de révision de texte
    - pymdownx.highlight            # Coloration syntaxique du code
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    true    #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Onglets coulissants.  === "Onglet"
        alternate_style: true       # Ajout depuis la version 8
    - pymdownx.superfences          # Imbrication de blocs.
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index: !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg
    - pymdownx.arithmatex:          # Formules en LaTeX 
        generic: true
        smart_dollar: false
   
    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format

plugins:
  - search # Inclure une barre de recherche
  - macros
  - mkdocs-jupyter: # Convertir les fichiers .ipynb et .py en pages du site https://github.com/danielfrg/mkdocs-jupyter
      include_source: True # Inclu une copie du fichier source.ipynb (ou .py) dans le dossier de la page sur le site pour son téléchargement

# extra:
 #- scripts_url: ../scripts/

extra_javascript:
  - javascripts/config.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - stylesheets/extra.css